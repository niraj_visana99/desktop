<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17-12-2018
 * Time: 01:12 PM
 */
session_start();
//if(isset($_SESSION['id']))
//{
//    echo "id is set and id = ".$_SESSION['id'];
//
//    exit();
//}

if(isset($_SESSION['user']))
{

    //    echo "This will be dashboard, You are authorized user";

    include ("./parts/head.php");
    include ("./parts/header.php");
    include ("./parts/menu.php"); ?>

    //

    <div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Zircos</a>
                            </li>
                            <li>
                                <a href="#">Dashboard</a>
                            </li>
                            <li class="active">
                                Dashboard
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row text-center">

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Statistics</p>
                            <h2 class="text-danger"><span data-plugin="counterup">34578</span></h2>
                            <p class="text-muted m-0"><b>Last:</b> 30.4k</p>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Patients Today</p>
                            <h2 class="text-dark"><span data-plugin="counterup">89</span> </h2>
                            <p class="text-muted m-0"><b>Last:</b> 1250</p>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Admitted This Month</p>
                            <h2 class="text-success"><span data-plugin="counterup">524</span></h2>
                            <p class="text-muted m-0"><b>Last:</b> 40.33k</p>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Request Per Day</p>
                            <h2 class="text-warning"><span data-plugin="counterup">65</span> </h2>
                            <p class="text-muted m-0"><b>Last:</b> 956</p>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Total Patients</p>
                            <h2 class="text-primary"><span data-plugin="counterup">3245</span> </h2>
                            <p class="text-muted m-0"><b>Last:</b> 20k</p>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">New Medicines</p>
                            <h2 class="text-danger"><span data-plugin="counterup">7854</span> </h2>
                            <p class="text-muted m-0"><b>Last:</b> 50k</p>
                        </div>
                    </div>
                </div><!-- end col -->

            </div>
            <!-- end row -->

        </div> <!-- container -->

    </div> <!-- content -->


    //


<?php include ("./parts/footer.php");
}

else
{
    echo "You are not authorized user, Please <a href='index.php'>login </a> First";
}

?>