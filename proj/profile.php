<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 18-12-2018
 * Time: 11:12 AM
 */
//print_r($_POST);
//exit();
session_start();
include ("./db/connection.php");

if(isset($_SESSION['user'])) {


//    echo "This will be dashboard, You are authorized user";

    include("./parts/head.php");
    include("./parts/header.php");
    include("./parts/menu.php");


//getting data to print onto fields
    $id = $_SESSION['id'];
    $query = "select * from users where id='$id'";
    $exe = mysqli_query($con, $query);
    $row = mysqli_fetch_assoc($exe);

    $name = $row['username'];
    $email = $row['email'];
    $pass = $row['password'];

    if (isset($_POST['submit']))
    {
        $name = $_POST['name'];
        $email = $_POST['email'];

        $password = $_POST['pass'];
        $hidden_pass=$_POST['pass_hidden'];


        if($password == $hidden_pass)
        {
            $query="update users SET username='".$name."', email='".$email."' where id='".$id."'";
            $exe=mysqli_query($con,$query);
        }
        else
        {

            $query="update users SET username='".$name."', email='".$email."',password='".md5($password)."' where id='".$id."'";
            $exe=mysqli_query($con,$query);
        }


        $_SESSION['user']=$name;


        if(mysqli_affected_rows($con)>0)
        {
            echo "updated rows";
        }
        else
        {
            echo "have not changed anything";
        }

        if(isset($_FILES['fileToUpload']))
        {


            $target_dir = "./image/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image

                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }

// Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
// Check file size
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
// Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
// Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

                    $query="update users SET path='".$target_file."' where id=$id";
                    $exe=mysqli_query($con,$query);

                    $_SESSION['path']=$target_file;

                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

        }


    }



?>



    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">



                <div class="row">
                    <div class="col-xs-12">
                        <div class="card-box">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="p-20">
                                    <h4 class="header-title m-t-0">Profile</h4>
                                    </div>


                                </div>


                                <div class="col-sm-12 col-xs-12 col-md-6">



                                    <div class="p-20">
                                        <form action="#" method="post" enctype="multipart/form-data" data-parsley-validate novalidate>
                                            <div class="form-group">
                                                <label for="userName">User Name<span class="text-danger">*</span></label>
                                                <input type="text" name="name" value="<?php echo $name; ?>" parsley-trigger="change" required
                                                       placeholder="Enter user name" class="form-control" id="userName">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailAddress">Email address<span class="text-danger">*</span></label>
                                                <input type="email" name="email" value="<?php echo $email; ?>" parsley-trigger="change" required
                                                       placeholder="Enter email" class="form-control" id="emailAddress">
                                            </div>

                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label">profile picture</label>
                                                    <input type="file" name="fileToUpload" class="filestyle" data-buttonname="btn-default" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);"><div class="bootstrap-filestyle input-group"><input type="text" class="form-control " placeholder="" disabled=""> <span class="group-span-filestyle input-group-btn" tabindex="0"><label for="filestyle-0" class="btn btn-default "><span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> <span class="buttonText">Choose file</span></label></span></div>
                                                </div>
                                            </div>


                                    </div>

                                </div>

                                <div class="col-sm-12 col-xs-12 col-md-6">
                                    <div class="p-20">

                                        <div class="form-group">
                                            <label for="pass1">Password<span class="text-danger">*</span></label>
                                            <input id="pass1" name="pass" value="<?php echo $pass; ?>" type="password" placeholder="Password" required
                                                   class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="passWord2">Confirm Password <span class="text-danger">*</span></label>
                                            <input data-parsley-equalto="#pass1" name="confirm_pass" value="<?php echo $pass; ?>" type="password" required
                                                   placeholder="Password" class="form-control" id="passWord2">
                                        </div>
                                        <input type="hidden" name="pass_hidden" value="<?php echo $pass; ?>">


                                    </div>

                                </div>


                                <div class="col-sm-12 col-xs-12 col-md-12">
                                    <div class="form-group m-b-0">
                                        <div class="p-20">
                                    <button name="submit" class="btn btn-primary waves-effect waves-light" type="submit">
                                        Submit
                                    </button>
                                    <button name="reset" type="reset" class="btn btn-default waves-effect m-l-5">
                                        Reset
                                    </button>

                                    </form>
                                        </div>
                                </div>
                                </div>


                            </div>
                            <!-- end row -->


                        </div> <!-- end ard-box -->
                    </div><!-- end col-->

                </div>
                <!-- end row -->


            </div> <!-- container -->

        </div> <!-- content -->

    </div>
















    <?php

    include ("./parts/footer.php");
}

else
{
    echo "You are not authorized user, Please <a href='index.php'>login </a> First";
}

?>