<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17-12-2018
 * Time: 02:56 PM
 */

require "./db/connection.php";
//echo "this is email ->";
$email=$_GET['key'];

if(isset($_POST['submit']))
{
    $pass=$_POST['pass'];
    $confirm_pass=$_POST['confirm_pass'];


    if($pass==$confirm_pass)
    {
        if(strlen($pass)<8)
        {
           // echo "must be 8 characters";

            ?>
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>Oh snap!</strong> password must be 8 characters
            </div>
            <?php
        }
        else
        {
            //change password in db

            $query="update `users` SET `password` = (MD5('$pass')) where email='$email'";
            //echo $query;
//            exit();
            $exe=mysqli_query($con,$query);
            if(mysqli_affected_rows($con)==1)
            {
                //echo"changed";
                ?>
                            <div class="alert alert-success" role="alert">
                                Password has been changed <br>
                                <a href="index.php" class="text-muted"><i class="fa fa-lock m-r-5"></i> Click here to login</a>

                            </div>
                            <?php
            }
            else
            {
                echo"something went wrong, please let us know in the feedback";
            }

        }

    }

    else
    {
       //echo "not matched";

        ?>
        <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="mdi mdi-block-helper"></i>
            <strong>Oh snap!</strong> Both password does not matched
        </div>
        <?php

    }

}


?>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- App title -->
    <title>Zircos - Responsive Admin Dashboard Template</title>

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../plugins/switchery/switchery.min.css">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>

</head>


<body class="fixed-left">


    <div class="col-xs-12">
        <div class="card-box">



<div class="p-20">
    <form action="#" method="post" data-parsley-validate novalidate>


        <div class="form-group">
            <label for="pass1">Password<span class="text-danger">*</span></label>
            <input name="pass" id="pass1" type="password" placeholder="Password" required="" class="form-control">
        </div>

        <div class="form-group">
            <label for="passWord2">Confirm Password <span class="text-danger">*</span></label>
            <input name="confirm_pass" data-parsley-equalto="#pass1" type="password" required="" placeholder="Password" class="form-control" id="passWord2">
        </div>

        <div class="form-group">

        <div class="form-group text-right m-b-0">
            <button name="submit" class="btn btn-primary waves-effect waves-light" type="submit">
                Submit
            </button>

        </div>
        </div>
    </form>
</div>
</div>
    </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="../plugins/switchery/switchery.min.js"></script>

    <script type="text/javascript" src="../plugins/parsleyjs/parsley.min.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('form').parsley();
        });
        $(function () {
            $('#demo-form').parsley().on('field:validated', function () {
                var ok = $('.parsley-error').length === 0;
                $('.alert-info').toggleClass('hidden', !ok);
                $('.alert-warning').toggleClass('hidden', ok);
            })
                .on('form:submit', function () {
                    return false; // Don't submit form for this demo
                });
        });
    </script>


</body>
</html>
