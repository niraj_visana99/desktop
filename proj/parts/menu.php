<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <div class="user-details">
                <div class="overlay"></div>
                <div class="text-center">
                    <img src="<?php echo $row['path'];?>" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div>
                        <a class="dropdown-toggle" aria-expanded="false"> Welcome ,<?php echo $row['username'];?> </a>
                    </div>
                </div>
            </div>

            <ul>
                <li class="menu-title">Navigation</li>

                <li>
                    <a href="dashboard.php" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a>
                </li>

                <li>
                    <a href="add_doctor.php" class="waves-effect"><i class="glyphicon glyphicon-user"></i><span> Add Doctor </span></a>
                </li>



            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

        <div class="help-box">
            <h5 class="text-muted m-t-0">For Help ?</h5>
            <p class=""><span class="text-dark"><b>Email:</b></span> <br/> support@support.com</p>
            <p class="m-b-0"><span class="text-dark"><b>Call:</b></span> <br/> (+123) 123 456 789</p>
        </div>

    </div>
    <!-- Sidebar -left -->

</div>
